//books module. contains books operations

//include mongoDB module
var mongo = require('mongodb');

//shortand
var server = new mongo.Server('127.0.0.1', 27017, {auto_reconnect: true});
var db = new mongo.Db('booksdb', server);
ObjectID = mongo.ObjectID;

//open the database
db.open(function(error, db) {
        
    if(!error) {
        
        //load the books collection
        db.collection("books", {strict:true}, function(error, collection) {
            if(error) {
                console.log("Book list not found.");
            } else {
                console.log("Book list found");
            }
        });
        
    } else {
        console.log("Connection error: " + error);
    }
});


//server functions

///////////////////////////////////////////////////////////////////////////////////////////////////////
//---[The user should be able to enter an ISBN number to retrieve details on the associated book]----//
///////////////////////////////////////////////////////////////////////////////////////////////////////

//retrieve book by ISBN number
exports.getByISBN = function(req, result) {
    
    //get url parameters
    var isbn = req.params.isbn;
    
    console.log('Searching for book with ISBN number: ' + isbn);
    
    db.collection('books', function(error, collection) {
                  
        //find single book in the database matching the isbn
        collection.findOne({'isbn': isbn}, function(error, item) {
            
            //send results
            result.send(item);
        });
    });
};

//retrieve list of all books
exports.getAll = function(req, res) {
    
    console.log('Aquiring list of all books.');
    
    db.collection('books', function(err, collection) {
                  
        //get entire database collection and put it into an array
        collection.find().toArray(function(err, items) {
                                  
            //send results
            res.send(items);
        });
    });
};
