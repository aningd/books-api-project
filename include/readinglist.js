//users module. contains user and user account operations

//include mongoDB module
var mongo = require('mongodb');

//shortand
var server = new mongo.Server('127.0.0.1', 27017, {auto_reconnect: true});
var db = new mongo.Db('booksdb', server);
ObjectID = mongo.ObjectID;

var ses;    //stores the session object

//open the database
db.open(function(error, db) {
        
    if(!error) {

    //load the books collection
    db.collection("reading_list", {strict:true}, function(error, collection) {
        if(error) {
            console.log("Reading list not found.");
        } else {
                console.log("Reading list found");
        }
        });

        } else {
            console.log("Connection error: " + error);
    }
});


//retrieve list of all books
exports.getAll = function(req, res) {
    
    //Get user registration data
    var user = req.params.user;
    var readingObj = [];
    
    console.log('Aquiring reading list for ' + user);
    
    db.collection('reading_list', function(err, collection) {
                  
        collection.find({'user': user}).toArray(function(error, entries) {
            console.log(entries);
            //get entire database collection and put it into an array
            for(var i = 0; i < entries.length; i++) {
                db.collection('books', function(err, books) {
                    console.log(entries[i].book);
                    //get entire database collection and put it into an array
                      books.findOne({"_id" : ObjectID(entries[i].book)}, function(error, item) {
                        
                        readingObj.push(item);
                    });
                    
                });
            }
            setTimeout(function(){res.send(readingObj);}, 500);
         });
    });
};


/////////////////////////////////////////////////////////////////////////////////
//---[3. The user should be able to add that book to their ‘reading list’.]----//
/////////////////////////////////////////////////////////////////////////////////

//adds a book to a user's reading list
exports.saveBook = function(req, result) {
    
    //Get user registration data
    var entry = req.body;
    
    console.log('Adding book to reading list: ' + entry);
    
    db.collection('reading_list', function(err, collection) {

        //Insert new reading list data into the database
        collection.insert(entry, {safe:true}, function(error, item) {
            if(error) {

                //entry creation unsuccessful
                result.send('error');
                console.log('error creating reading list entry');
            } else {

                //entry creation successful
                console.log('New reading list entry added');
                result.send('success');    //send result
            }
        });
    });
}

//removes a book to a user's reading list
exports.removeBook = function(req, res) {
    
    //Get user registration data
    var entry = req.body;

    console.log('Remove book from reading list: ' + entry);

    db.collection('reading_list', function(err, collection) {
        collection.remove({'book': entry.book}, {safe:true}, function(err, result) {
            if (err) {
                res.send('error');
            } else {
                console.log('entries deleted: ' + result);
                res.send('success');
            }
        });
    });
}