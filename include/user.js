//users module. contains user and user account operations

//include mongoDB module
var mongo = require('mongodb');

//shortand
var server = new mongo.Server('127.0.0.1', 27017, {auto_reconnect: true});
var db = new mongo.Db('booksdb', server);
ObjectID = mongo.ObjectID;


//open the database
db.open(function(error, db) {

    if(!error) {

        //load the books collection
        db.collection("users", {strict:true}, function(error, collection) {
            if(error) {
                console.log("Users not found.");
            } else {
                console.log("Users found");
            }
        });

    } else {
        console.log("Connection error: " + error);
    }
});


///////////////////////////////////////////////////////////////////////
//---[1. The user should be able to create an account and log in]----//
///////////////////////////////////////////////////////////////////////

//adds a new user to the database
exports.add = function(req, res) {
    
    //Get user registration data
    var user = req.body;
    
    console.log('Creating new user: ' + JSON.stringify(user));
    
    db.collection('users', function(err, collection) {
                  
        //Insert new user data into the database
        collection.insert(user, {safe:true}, function(error, result) {
            if(error) {
                
                //user creation unsuccessful
                res.send('error');
            } else {
                          
                //user creation successful
                console.log('New user has been created');
                res.send('success');    //send result
            }
        });
    });
}

//user login crediential check
exports.login = function(req, result) {
    
    //Get user registration data
    var user = req.body;

    console.log('Logging in user: ' + JSON.stringify(user));

    db.collection('users', function(err, collection) {

        collection.findOne({'username': user.username}, function(error, item) {
           //general error
           if(error) {
                result.send('invalid');
                console.log('error');
           } else {
               //password comparison successful
               if(item.password == user.password) {
                    result.send(item._id);
               } else { //passwords don't match
                    result.send('invalid');
                    console.log('invalid');
               }
           }
        });
    });
}
