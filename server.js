
//includes
var express = require('express'),                //node express module
users = require('./include/user'),
readinglist = require('./include/readinglist'),                
books = require('./include/books');            //books module

//create application object
var app = express();

app.use(express.logger("dev"));     
app.use(express.bodyParser());      //use express body parser to data from POST

//Additional headers need for cross domain requests in browsers
app.use(function (req, res, next) {
        
    //allow requests from this address only
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

//GET
app.get('/books', books.getAll);                //gets list of all books
app.get('/books/:isbn', books.getByISBN);       //gets a book with the given isbn

//POST
app.post('/users', users.add);                  //creates a new user
app.post('/users/login', users.login);          //logs a user in by checking credentials
app.get('/readinglist/:user', readinglist.getAll);
app.post('/readinglist', readinglist.saveBook);       //adds a given book to the user's reading list
app.post('/readinglist/remove', readinglist.removeBook);   //removes a given book from the user's reading list


app.listen(3000);